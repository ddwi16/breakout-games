package com.example.breakoutgame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.breakoutgame.BreakoutGame;
import com.example.breakoutgame.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPlay = findViewById(R.id.play_btn);
        btnPlay.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play_btn:
                startActivity(new Intent(MainActivity.this, BreakoutGame.class));
                break;
        }
    }
}
